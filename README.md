# Password Generator


## Video Demo

https://youtu.be/J6a4yfy9eXc


## About

This is a simple password generator created with Python, using the Flask library.
HTML, CSS, and Bootstrap are also involved in this application.

The app can be hosted on a system and accessed from other systems via Internet, or simply run it on your own computer and access it from the localhost. This is a web app, not a native GUI application.

## Getting started

To run this app on your current system, you will have to install Python3, Pip, and the Flask Python Library. You may do this in a Python virtual environment if you wish.

Once the entire directory is on the desired system, you will need to run the Python file in the top-level folder. This can be done with `python3 [filename]` or using chmod and just running it with `./`.

The application will then be accessible.

## Documentation

To use the Password Generator, simply press the blue "Generate Password" button at the bottom, once you've chosen the checkboxes.

You may notice that some checkboxes are already enabled, and that's because they're the default recommended values. You may change them if you wish.

### Lowercase Letters

Enables/Disables the use of lowercase letters. This only includes the 26 lowercase letters from the standard English keyboard layout. (qwertyuiopasdfghjklzxcvbnm)

### Uppercase Letters

Enables/Disables the use of uppercase letters. This only includes the 26 uppercase letters from the standard English keyboard layout. (QWERTYUIOPASDFGHJKLZXCVBNM)

### Numbers

Enables/Disables the use of numbers. (1234567890)

### Symbols

Enables/Disables the use of symbols. (!@#$%^&*()-=_+`~{}[]|\:;"'<,>.?/)

### Spaces

Enables/Disables the use of spaces. You may notice that, in the current version, spaces rarely appear in your passwords. That is because the probability of a space getting into the password is not heightened or altered; this means that the space character has the same probability as all the other characters (a 1 in around 95 chance).

### Increase Memorability

This option attempts making the password easier to read and memorize by humans. It will include 1-2 English words from a 3001-word list. As of the Beta version, password lengths that are around 8 or lower may not always work correctly, i.e. the number of characters could be larger than desired.

As of the Beta version, the words will appear toward the beginning of the password. **This is only noticeable when the password length is larger than 15-20 characters.**

### Length 

This option lets you choose how long the password is (how many characters are in it). There is technically no limit, but extremely large numbers can crash the application or slow down your system while it's creating them.
